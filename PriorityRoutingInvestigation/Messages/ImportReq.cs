﻿namespace Messages
{
  public interface IWithPriority
  {
    int Priority {get;}
  }

  public class ImportReq : IWithPriority
  {
    public string Id {get; set;}
    public int Priority {get; set;}

    public string Firstname {get; set;}
    public string Lastame {get; set;}

    public override string ToString()
    {
      return $"{Firstname} {Lastame} has priority {Priority}";
    }
  }
}
