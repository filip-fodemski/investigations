﻿using MassTransit;
using Messages;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PriorityRoutingHandler
{
  public class ImportReqWithPriority1Consumer : IConsumer<ImportReq>
  {
    public Task Consume(ConsumeContext<ImportReq> context)
    {
      Debug.WriteLine($"Priority 1 handler: {context.Message}");

      return Task.CompletedTask;
    }
  }
}
