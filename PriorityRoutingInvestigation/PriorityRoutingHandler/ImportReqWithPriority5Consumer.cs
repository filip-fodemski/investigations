﻿using MassTransit;
using Messages;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PriorityRoutingHandler
{
  public class ImportReqWithPriority5Consumer : IConsumer<ImportReq>
  {
    public Task Consume(ConsumeContext<ImportReq> context)
    {
      Debug.WriteLine($"Priority 5 handler: {context.Message}");

      return Task.CompletedTask;
    }
  }
}
