﻿using MassTransit;
using Messages;
using RabbitMQ.Client;
using System;

namespace PriorityRoutingHandler
{
  class Program
  {
    static void Main(string[] args)
    {
      try
      {
        var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
        {
          cfg.Host(new Uri("rabbitmq://localhost/"), h =>
          {
              h.Username("user");
              h.Password("passwd");
          });

          cfg.ReceiveEndpoint("priority-5", x =>
          {
              x.ConfigureConsumeTopology = false;

              x.Consumer<ImportReqWithPriority5Consumer>();

              x.Bind(nameof(ImportReq), s => 
              {
                  s.RoutingKey = 5.ToString();
                  s.ExchangeType = ExchangeType.Direct;
              });
          });

          cfg.ReceiveEndpoint("priority-1", x =>
          {
              x.ConfigureConsumeTopology = false;

              x.Consumer<ImportReqWithPriority1Consumer>();

              x.Bind(nameof(ImportReq), s => 
              {
                  s.RoutingKey = 1.ToString();
                  s.ExchangeType = ExchangeType.Direct;
              });
          });
        });

        busControl.Start();

        Console.WriteLine("Press <enter> exit...");
        Console.ReadLine();

        busControl.Stop();
      }
      catch (Exception ex)
      {
        Console.WriteLine($"Exception: {ex}");
      }
    }
  }
}
