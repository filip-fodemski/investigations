﻿using MassTransit;
using Messages;
using RabbitMQ.Client;
using System;

namespace PriorityRoutingSender
{
  class Program
  {
    static void Main(string[] args)
    {
      try
      {
        var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
        {
          cfg.Host(new Uri("rabbitmq://localhost/"), h =>
          {
              h.Username("user");
              h.Password("passwd");
          });

          cfg.Send<ImportReq>(x =>
          {
              x.UseRoutingKeyFormatter(context => context.Message.Priority.ToString());
          });

          cfg.Message<ImportReq>(x =>
          {
            x.SetEntityName(nameof(ImportReq));
          });

          cfg.Publish<ImportReq>(x => x.ExchangeType = ExchangeType.Direct);
        });

        busControl.Start();

        Console.WriteLine("Press <enter> to send priority 1 message...");
        Console.ReadLine();

        busControl.Publish(new ImportReq { Priority = 1, Firstname = "Bart", Lastame = "Simpson"});

        Console.WriteLine("Press <enter> to send priority 5 message...");
        Console.ReadLine();

        busControl.Publish(new ImportReq { Priority = 5, Firstname = "Lisa", Lastame = "Simpson"});

        Console.WriteLine("Press <enter> exit...");
        Console.ReadLine();
 
        busControl.Stop();
      }
      catch (Exception ex)
      {
        Console.WriteLine($"Exception: {ex}");
      }
    }
  }
}
