# README #

This repository is for investigations of new technology, tools and for proof-of-concept work.

### Contents ###

The following C# Visual Studio solutions can be found in this repo:

* PriorityRoutingInvestigation - contains proof-of-concept of routing MassTransit and RabbitMQ messages based on message content.
